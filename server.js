var express = require('express');
var app = express();

app.get('/*', function (req, res) {
  res.sendfile(__dirname + '/app/development' + req.url);
});

app.listen(3000, function () {
  console.log('Example app listening on port 3000!');
});