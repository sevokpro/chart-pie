(function () {
    angular.module('app').directive('myChartpie', [function () {
        return {
            templateUrl: '/js/angular/components/MyChartPie/pie.html',
            replace: true,
            restrict: 'E',
            scope: {
                data: '@',
                uplink: '@'
            },
            controller: function($scope, $element, $timeout, $http) {

                var elem = $element[0].querySelector('#myChart');
                elem.height = elem.width;
                var ctx = elem.getContext("2d");

                //init object
                $scope.chart = {};

                $scope.chart.uplink = $scope.uplink;

                //get data from server
                $scope.chart.get_server_data = function () {
                    var data = JSON.parse($scope.data);
                    return data;
                }

                //add to server data colors
                $scope.chart.compile_data = function () {
                    var server_data = $scope.chart.get_server_data();
                    var compiled_data = [
                        {
                            color:"#f44336",
                            rgb_color: "rgb(244, 67, 54)",
                            highlight: "#e57373",
                            rgb_highlite: "rgb(229,115,115)",
                            label:"1"
                        },{
                            color: "#673ab7",
                            rgb_color: "rgb(103,58,183)",
                            highlight: "#9575cd",
                            rgb_highlite: "rgb(149,117,205)",
                            label:"2"
                        },{
                            color: "#2196f3",
                            rgb_color: "rgb(33,150,243)",
                            highlight: "#64b5f6",
                            rgb_highlite: "rgb(100,181,246)",
                            label:"3"
                        },{
                            color: "#4caf50",
                            rgb_color: "rgb(76,175,80)",
                            highlight: "#81c784",
                            rgb_highlite: "rgb(129,199,132)",
                            label:"4"
                        },{
                            color: "#ffc107",
                            rgb_color: "rgb(255,193,7)",
                            highlight: "#ffd54f",
                            rgb_highlite: "rgb(255,213,79)",
                            label:"5"
                        }
                    ]
                    for (var i = server_data.length-1; i >= 0; i--){
                        compiled_data[i]['value'] = server_data[i];
                    }
                    return compiled_data;
                }


                //get full data for chart
                $scope.chart.data = $scope.chart.compile_data();

                //create chart from data and options
                $scope.chart.link = new Chart(ctx).Pie(
                    $scope.chart.data,
                    {
                    responsive: true
                });

                //set errors/block update
                $scope.chart.block_update_view = false;
                $scope.chart.update_error = false;

                //method to update chart
                $scope.chart.update = function () {
                    if ($scope.chart.validate() === false){
                        return false;
                    }

                    var check = $scope.chart.get_sum();
                    if (check === 100){
                        //throw error if sum not 100
                        $scope.chart.update_error = false;

                        //block update more that 1 update per second
                        if (!$scope.chart.block_update_view){
                            $scope.chart.link.update();
                            
                            $scope.chart.block_update_view = true;
                            $timeout(function() {
                                $scope.chart.block_update_view = false;
                                $scope.chart.link.update();
                            }, 1000);
                        }
                    } else {
                        $scope.chart.update_error = true;
                    }
                }
                    
                $scope.chart.validate = function () {
                    angular.forEach($scope.chart.link.segments, function (item) {
                       if (typeof(item.value) !== 'number'){
                            return false
                       }     
                    })
                }


                // method to get sum from all labels
                $scope.chart.get_sum = function () {
                    var current_value = 0;

                    angular.forEach($scope.chart.link.segments, function (item) {
                       current_value += +item.value;               
                    })
                    return current_value; 
                }

                //method to serializate chart data
                $scope.chart.save = function () {
                    var out = [];
                    angular.forEach($scope.chart.link.segments, function (item) {
                        out.push({
                            value: item.value,
                            label: item.label
                        })                        
                    })

                    var req = {
                        method: 'POST',
                        url: $scope.chart.uplink,
                        headers: {
                            'Content-Type': 'JSON'
                        },
                        data: {
                            d: JSON.stringify(out) 
                        }
                    }

                    $http(req).then(function successCallback(response) {
                        console.log('данные успешно отправлены на сервер');
                    }, function errorCallback(response) {
                        console.log('при отправке данных произошла ошибка');
                        console.log(response)
                    });
                }

                angular.element(document).ready(function () {
                    var circles = $element[0].querySelectorAll('.circle');
                    angular.forEach(circles, function (item, key) {
                        item.style.backgroundColor = $scope.chart.data[key].rgb_color;
                    })
                });
                // update chart to throw error
                $scope.chart.update();
            },
            // compile: function compile(tElement, tAttrs, transclude) {
            //     return function postLink(scope, iElement, iAttrs, controller) {
    
            //     }
            // },
            link: function postLink(scope, iElement, iAttrs) {
                
            }
        };
    }])
})(); 
