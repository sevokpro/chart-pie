to view example clone repositary and write "node server.js"
server will start on localhost:3000

dependencies:


```
#!JSON

["dependencies":{
    "angular":"1.4.7",
    "bootstrap": "3.3.6",
    "Chart.js": "1.0.2"
  }]
```

how to link?

```
#!HTML

<script type="text/javascript" src="/js/main.js"></script>
<script type="text/javascript" src="/js/angular/components/MyChartPie/pie.js"></script>
```

how to use?

write


```
#!HTML

<my:chartpie></my:chartpie>
```

in your html code and then in this place you will see chart